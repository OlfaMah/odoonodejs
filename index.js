const express = require ('express');
const app = express();
const port = process.env.PORT || 5000;
const Odoo = require ('odoo-xmlrpc');
const cors = require ('cors');
const bodyparser = require ("body-parser");
app.use(bodyparser.json());

app.use(cors());

app.listen(port,()=> console.log(`Listening on port ${port}`));

//connect to Odoo 

const odoo = new Odoo({
    url : `http://localhost:8069`,
    db : `PFE`,
    username: `***********`,
    password : `*************`
});

  odoo.connect(function(err){
        if(err){
            console.log(err)
        }
    console.log("connected to odoo server");
//Liste fournisseur par odoo
//Liste Client par odoo 
//Liste des commandes 
app.get('/liste_Commandes',(req, res)=> {
    let inParams = [];
    let domain = [['type',"=",'out_invoice'] ]

    inParams.push(domain);
    let params= [];

    params.push(inParams);
   -
    params.push({
        fields : ['name','reference','date_invoice','date_due','amount_untaxed_signed','amount_total_signed'],
        limit : 100
    })
    odoo.execute_kw(
        'account.invoice',
        'search_read',
        params,
        function(err, value){
            if(err) {
                return console.log(err);
            }
            res.send({
                Commandes : value
            })
        }
    )
})
//Total Vente
//Total Matiere premiere

app.get('/fournisseurs_lists', (req, res)=> {
  
    let inParams = [];
    let domain = [['active',"=",true] ]

    inParams.push(domain);
    let params= [];

    params.push(inParams);
   
    params.push({
        fields : ['name', 'display_name','function','date_begin','date_end'],
        limit : 100
    })
    odoo.execute_kw(
        'res.partner',
        'search_read',
        params,
        function(err, value){
            if(err) {
                return console.log(err);
            }
            res.send({
                Fournisseur : value
            })
        }
    )
    });
//Total Prix Matiére Premiére
app.get('/CountRecord',(req ,res) =>{
    let inParams = [];
    let domain = [['procure_method',"=","make_to_stock"]]

    inParams.push(domain);
    let params= [];

    params.push(inParams);
    params.push({
        fields : ['name','product_qty','value'],
        limit : 100,
       
    })
    odoo.execute_kw(
        'stock.move',
        'search_read',
        params,
        function(err, value){
            if(err) {
                return console.log(err);
            }
            console.log("Processing func -> Afficher la somme Total des matiere premiere");
            var x = value.length;
            console.log(x);
            const sommeValue = [0];
            const reducer = (accumulator, currentValue) => accumulator + currentValue;
            var  somme = 0;
            for (var i=0; i<x ; i++){
                somme = somme +sommeValue.reduce(reducer,value[i].value);
                console.log(sommeValue.reduce(reducer,value[i].value));
           
            }
            res.send({
                Stock : value , SommeTotal : somme
            })
        }
    )
})
//Stock inventory avec qte stock stock_move
app.get('/stockInventory',(req ,res)=>{
    let inParams = [];
    let domain = [['procure_method',"=","make_to_stock"]]

    inParams.push(domain);
    let params= [];

    params.push(inParams);
    params.push({
        fields : ['id','name','product_uom_qty','value'],
        limit : 100,
       
    })
    odoo.execute_kw(
        'stock.move',
        'search_read',
        params,
        function(err, value){
            if(err) {
                return console.log(err);
            }

            res.send({
                Stock : value
            })
        }
    )

})

app.get('/article_lists',(req ,res) => {
    let inParams = [];
    let domain = [['active',"=",true], ['type','=','product']]

    inParams.push(domain);
    let params= [];

    params.push(inParams);
   
    params.push({
        fields : ['name','default_code','description','type','list_price','barcode','volume','weight','create_date'],
        limit : 100,
       
    })
    odoo.execute_kw(
        'product.template',
        'search_read',
        params,
        function(err, value){
            if(err) {
                return console.log(err);
            }

            res.send({
                Aricles : value
            })
        }
    )
    });
})

app.post('/createDataPartner',(req,res)=>{
    const {name } = req.body;
    var inParams = [];
    inParams.push({name});
    var params = [];
    params.push(inParams);

    odoo.execute_kw('res.partner','create',params,function(err,value){
        if(err){
            return console.log(err);
        }
        console.log('Result',value);
    });
})
app.post('/updateArticle/:id',(req,res)=>{
    const {id} = req.params;
    const { name,default_code,description,type,list_price,barcode,volume,weight} = req.body;
    var inParams = [];
    inParams.push([id]);
    inParams.push({name,default_code,description,type,list_price,barcode,volume});
    var params = [];
    params.push(inParams);
    odoo.execute_kw('product.template','write',params,function(err,value){
        if(err){
            return res.status(400).send({message: "erreur"+err})
        }
        return res.status(400).send({result: value,success :"true"})
    });
})
app.post('/deleteArticle/:id',(req,res)=>{
    const {id} = req.params;
    var inParams = [];
    inParams.push([id]);
    var params = [];
    params.push(inParams);
    odoo.execute_kw('res.partner', 'unlink', params, function (err, value) {
        if (err) { return console.log(err); }
        console.log('Result: ', value);
    });
})
app.post('/createArticle',(req,res)=>{
    const { name,default_code,description,type,list_price,
        barcode,volume,weight} = req.body;
    var inParams = [];
    inParams.push({name,default_code,description,type,list_price,barcode,volume,weight});
    var params = [];
    params.push(inParams);
    odoo.execute_kw('product.template','create',params,function(err,value){
        if(err){
            return res.status(400).send({message: "erreur"+err})
        }
        return res.status(400).send({result: value,success :"true"})

        //console.log('Result',value);
    });
})
//Report printing
app.get('/reportPrint',(req, res)=>{
    
    var inParams = [];
    inParams.push([['is_company', '=', true],['customer', '=', true], ['name','=','FFnew Partner']]);
    inParams.push(10); //offset
    inParams.push(5);  //limit
    var params = [];
    params.push(inParams);
    odoo.execute_kw('res.partner', 'search', params, function (err, value) {
        if (err) { return console.log(err); }
        console.log('Result: ', value);
        res.status(200).send({data : value})
    });
})
//Create new parnter in odoo 
app.get('/createData',(req, res)=>{
    var inParams = [];
    //Création d'un nouveau res partner 
    inParams.push({'name':'FFnew Partner'});

    var params = [];
    params.push(inParams);

    odoo.execute_kw('res.partner','create',params,function(err,value){
        if(err){
            return console.log(err);
        }
        console.log('Result',value);
    });
});
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
	res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
	next();
  });


  app.get('/reportPrinting',(req,res)=>{
  /*  var inParams = [];
    inParams.push([['type', '=', 'out_invoice'], ['state', '=', 'open']]);
    var params = [];
    params.push(inParams);
    odoo.execute_kw('account.invoice', 'search_read', params, function (err, value) {
        if (err) { return console.log(err); }
        if(value){
            var params = [];
            params.push(value);
            odoo.render_report('account.report_invoice', params, function (err2, value2) {
                if (err2) { return console.log(err2); }
                console.log('Result: ' + value2);
            });
        }
    });*/

});
